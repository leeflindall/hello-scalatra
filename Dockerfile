FROM centos:7
MAINTAINER you
RUN yum -y install wget && wget http://mirrors.ukfast.co.uk/sites/ftp.apache.org/tomcat/tomcat-8/v8.5.45/bin/apache-tomcat-8.5.45.tar.gz && tar xf /apache-tomcat-8.5.45.tar.gz && mv apache-tomcat-8.5.45 /opt/tomcat
RUN yum -y install java-1.8.0-openjdk
COPY hello-scalatra.war /opt/tomcat/webapps
EXPOSE 8080
CMD ["/opt/tomcat/bin/catalina.sh","run"]
